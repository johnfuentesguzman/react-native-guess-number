import React, {useState} from 'react';
import { StyleSheet, View, SafeAreaView } from 'react-native';
import  Header from './components/Header';
import StartGamePage from './pages/StartGamePage'
import GamePage from './pages/GamePage'

export default function App() {
  const [selectedNumber, setSelectedNumber] = useState('');
  const [isGameStarted, setGameStarted] = useState(false);

  const startGameHandler = (isGameStarted,userNumberGuess) => {
    setGameStarted(isGameStarted);
    setSelectedNumber(userNumberGuess)
  };

  return (
    <SafeAreaView style={styles.screen}>
          <Header/>
          { !isGameStarted && selectedNumber === '' ? <StartGamePage startGameHandler={startGameHandler.bind(this)} /> : <GamePage userGuessNumber={selectedNumber}/>  }
    </SafeAreaView>
  );
} 
const styles = StyleSheet.create({
  screen: {
    flex: 1
  },
});
