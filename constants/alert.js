export default  {
    errorType: 'error',
    confirmType: 'confirm',
    winType: 'win',
    title: 'Guess Number',
    confirmNumberMss: 'Do do you want to use this number to Play?',
    cancelButton: 'Cancel',
    confirmButton: 'Lets PLay',
    cheating: 'Please, don\'t cheat in the game',
    win: 'CONGRATULATIONS, You win',
    winButtonText: 'Great'
}