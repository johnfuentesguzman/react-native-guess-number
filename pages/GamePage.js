import React, { useState, useRef, useEffect } from 'react'
import { View, Text, StyleSheet, FlatList, Dimensions } from 'react-native';
import NumberContainer from '../components/NumberContainer';
import AlertMessage from '../components/AlertMessage';
import ButtonString from '../constants/buttons';
import TextString from '../constants/text'
import AlertString from '../constants/alert';
import GenerateRandomNumberBetween from '../utils/GenerateRandomNumber';
import CustomButton from '../components/CustomButton';
import { Ionicons } from '@expo/vector-icons';
import Colors from '../constants/colors';
import getDimensions from '../utils/getDimensions'

export const GamePage = (props) => {
    const { userGuessNumber } = props;
    const [appCurrentGuess, setAppCurrentGuess] = useState('');
    const [passGuess, setPassGuess] = useState([]);
    const [buttonContainerWidth, setButtonContainerWidth]  = useState('8%');
    let newAppNumber = '';

    if (userGuessNumber && !appCurrentGuess) {
        newAppNumber = GenerateRandomNumberBetween(1, 100, parseInt(userGuessNumber)) // gerating app/computer guess
    }

    const currentLowestChoice = useRef(1); // using 'useRef' because it allows the data prevail/keep when the component is re-rendered
    const currentGreatestChoice = useRef(100);
    newAppNumber && setAppCurrentGuess(newAppNumber);

    const orientationListener = () => {
        const currentDimension = getDimensions();
        return currentDimension === 'portrait' ? setButtonContainerWidth('8%') : setButtonContainerWidth('12%')
    }

    useEffect(() => {
        orientationListener();
        if (appCurrentGuess && (appCurrentGuess === userGuessNumber)) {
            AlertMessage(AlertString.winType, AlertString.win, null);
        }

        Dimensions.addEventListener('change', orientationListener);
        return () => { // this return every single time when the page renders and useffect is ativated, it will remove the previous listener and will call a new one
            Dimensions.removeEventListener('change', orientationListener); 
        };
    }, [appCurrentGuess, userGuessNumber]) // use effect hook will be recall once of the depencies have changed (into array)

    const setNextGuessHandler = (direction) => {
        if (direction === ButtonString.lower && appCurrentGuess < userGuessNumber || direction === ButtonString.greater && appCurrentGuess > userGuessNumber) {
            AlertMessage(AlertString.errorType, AlertString.cheating, null);
            return;
        }
        if (direction === ButtonString.lower) {
            currentGreatestChoice.current = appCurrentGuess; // .current is a property of 'useRef' that save a value when the component is re-rendered 
        } else {
            currentLowestChoice.current = appCurrentGuess;
        }
        const nextAppNumber = GenerateRandomNumberBetween(currentLowestChoice.current, currentGreatestChoice.current, appCurrentGuess);
         //nextAppNumber && guessList.push({ id: Math.floor((Math.random() * 10) + 1), number: nextAppNumber });
        if(nextAppNumber){
            setAppCurrentGuess(nextAppNumber);
            setPassGuess(currentGuess => [...currentGuess, nextAppNumber]); // this spread in that position  , it means we collected the older number fist
        }
    };

    const renderListItem = (length, value) => 
    (
        <View key={Math.floor((Math.random() * 100) + 1)} style={styles.listItem}>
            <Text>#{length - value.index}</Text>
            <Text>{value.item}</Text>
        </View>
    );


    return (
        <View style={styles.screen}>
            <Text>{TextString.gamePageTitle}</Text>
            <NumberContainer guessNumber={appCurrentGuess} />
            <View style={{...styles.buttonContainer, ...{height:buttonContainerWidth}}}>
                <CustomButton buttonStyle={styles.buttonSuccess} press={setNextGuessHandler.bind(this, ButtonString.lower)}>
                    <Ionicons name="ios-add-circle" size={24} color="white" />
                </CustomButton>
                <CustomButton buttonStyle={styles.buttonWarning} press={setNextGuessHandler.bind(this, ButtonString.greater)} >
                    <Ionicons name="ios-remove-circle" size={24} color="white" />
                </CustomButton>
            </View>
            <View style={styles.listContainer}>
{/*                 <ScrollView contentContainerStyle={styles.scrollViewContainer}>
                    {passGuess && passGuess.map( (guess,index) => renderListItem(guess,index + 1))}
                </ScrollView> */}
                <FlatList  contentContainerStyle={styles.flatListViewContainer} keyExtractor={(item) => item}  data={passGuess} renderItem={renderListItem.bind(this,passGuess.length + 1)}/>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        padding: 10,
        alignItems: 'center'
    },
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: "space-between",
        marginTop: 20,
        width: Dimensions.get('window').width > 350 ? '70%' : Dimensions.get('window').width / 1.2, // window just matters in android, because using it we are excluding the status bar of the size count
    },
    buttonSuccess: {
        flex: 1,
        backgroundColor: Colors.success,
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonWarning: {
        backgroundColor: Colors.danger,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    listContainer: {
        flex: 1,
        width: '60%'
    },
    flatListViewContainer:{
        flexGrow: 1, // i didnt use flex because , this allows to items how much space they are able to take , but respecting the expected behaviour for scroll
        justifyContent: 'flex-end'
    },
    listItem:{
        borderColor: '#ccc',
        borderWidth: 1,
        padding: 15,
        marginVertical: 10,
        backgroundColor: 'white',
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%'
    }
});

export default GamePage;
