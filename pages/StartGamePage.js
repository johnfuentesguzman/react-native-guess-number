import React, { useState } from 'react';
import { StyleSheet, Text, View, TextInput, Button, TouchableWithoutFeedback, Keyboard, ScrollView, KeyboardAvoidingView } from 'react-native';
import Colors from '../constants/colors';
import AlertString from '../constants/alert';
import Input from '../components/Input';
import NumberSelected from '../components/NumberSelected';
import AlertMessage from '../components/AlertMessage';

export const StartGamePage = (props) => {
    const { startGameHandler } = props; // (App.js) -> function as props/parameter to handler what page wi will show: StartGamepage or GamePage

    const [enteredValue, setEnteredValue] = useState('');
    const [selectedNumber, setSelectedNumber] = useState('');
    const [confimedNumber, setConfirmedNumber] = useState(false);

    const numberInputHandler = (inputValue) => {
        setEnteredValue(inputValue);
    };

    const resetEnteredValue = () => {
        setEnteredValue('');
    };

    const textConfig = {
        title: 'Welcome, Start a New Game',
        selectNumber: 'Please, Select a Number',
        resetButton: 'Reset',
        playButton: 'Play'
    };


    const submitNumber = () => {
        setSelectedNumber(parseInt(enteredValue));
        setConfirmedNumber(true);
        startGameHandler(true, parseInt(enteredValue));  //Once the user has confirmed that wants to play we show the gamePage (go to app.js)
        setEnteredValue(''); // clear the number in textinput and state
    };

    const onConfirmNumber = () => {
        const chosenNumber = parseInt(enteredValue);
        if (chosenNumber != NaN && chosenNumber <= 0 && chosenNumber > 99) {
            return
        }
        AlertMessage(AlertString.confirmType, AlertString.confirmNumberMss, submitNumber.bind(this));
    };
    return (
        <ScrollView>
            <KeyboardAvoidingView behavior='position' keyboardVerticalOffset={30}>
                <TouchableWithoutFeedback onPress={() => { Keyboard.dismiss() }}>
                    <View style={styles.screen}>
                        <View style={styles.inputContainer}>
                            <Text style={styles.welcomeTitle}> {textConfig.title} </Text>
                            <Text>{textConfig.selectNumber}</Text>
                            <Input styleSheet={styles.input} val={enteredValue} length={2} correct={false} capitalize='none' keyboard='numeric' numberInputHandler={numberInputHandler.bind(this)} />
                            <View style={styles.buttonContainer}>
                                <View style={styles.buttonSucess}>
                                    <Button onPress={() => onConfirmNumber()} title={textConfig.playButton} color={Colors.success} />
                                </View>
                                <View style={styles.buttonWarning}>
                                    <Button title={textConfig.resetButton} color={Colors.warning} onPress={() => resetEnteredValue()} />
                                </View>
                            </View>
                        </View>
                        {confimedNumber && <NumberSelected selectedNumber={selectedNumber} />}
                    </View>
                </TouchableWithoutFeedback>

            </KeyboardAvoidingView>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    screen: {
        flex: 1, // take all space it can take
        width: '100%',
        justifyContent: 'flex-start',
        padding: 10, // all the edges
    },
    inputContainer: {
        height: '50%',
        width: '100%',
        alignContent: 'center',
        alignItems: 'center',
        shadowColor: 'black', // just work on ios
        shadowOffset: { width: 0, height: 2 }, // just work on ios
        shadowRadius: 6, // just work on ios
        shadowOpacity: 0.26, // just work on ios
        elevation: 5, // shadows .just on Android
        backgroundColor: 'white',
        borderRadius: 10 // round the curves/edges
    },
    welcomeTitle: {
        textAlign: 'center',
        fontSize: 20,
        marginVertical: 15 // it replaces 'margin bottom' and 'margin top'
    },
    selectNumber: {
        textAlign: 'right',
        fontSize: 20,
        marginVertical: 10 // it replaces 'margin bottom' and 'margin top'
    },
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: "space-between",
        paddingHorizontal: 15 // padding left and padding right (because we are in ROW as direction)
    },
    buttonSucess: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonWarning: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    input: {
        width: '70%',
        height: 60,
        paddingTop: 25
    },
    selectedNumberContainer: {
        marginTop: 20
    }
});

export default StartGamePage;