import React from 'react';
import {Alert} from 'react-native';
import AlertText from '../constants/alert'

export const AlertMessage = (type, mss,press, buttonText) => {
    const buttonTitle = buttonText ? buttonText : AlertText.cancelButton;

    var buttonsArr = [{text: buttonTitle, onPress: () => console.log('submitt canceled'), style: 'cancel'}];
    if(type != AlertText.errorType && type !=AlertText.winType){
        buttonsArr.push({text: AlertText.confirmButton, onPress: () => press()},)
    }
    var alertMess = Alert.alert(
        AlertText.title,
        mss,
        buttonsArr,
        { cancelable: false }
    )

    return  alertMess;
    
}
export default AlertMessage;