import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import NavigationBar from 'react-native-navbar';

export const Header = () => {
    const titleConfig = {
        title: 'Guess Number',
    };
    return (
        <View style={styles.headerContainer}>
            <Text style = {styles.headerTitle}>{titleConfig.title}</Text> 
        </View>
    )
};

const styles = StyleSheet.create({
    headerContainer: {
        flexDirection: 'row',
        width: '100%',
        height: 90,
        backgroundColor: '#f7287b',
        alignItems: 'center',
        justifyContent: 'center'
    },
    headerTitle: {
        color: 'black',
        paddingTop: 36,
        fontSize: 18
    }
});
export default Header;

