import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Colors from '../constants/colors';

export const NumberSelected = (props) => {
    const { selectedNumber } = props;
    return (
        <View style={styles.container}>
            <View style={styles.dataContainer}>
                <Text style={styles.numberSelectedTitle}>You selected</Text>
                <Text style={styles.numberSelectedValue}>{selectedNumber}</Text>
            </View>
        </View>
    );

}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        height: '12%',
        paddingTop: 10,
    },
    dataContainer:{
        width: '50%',
        borderColor: Colors.warning,
        padding: 0,
        borderRadius: 10,
        marginVertical: 10,
        borderWidth: 2,
    },
    numberSelectedTitle:{
        textAlign: 'center'
    },
    numberSelectedValue: {
        color: Colors.warning,
        fontSize: 22,
        textAlign: 'center'
    }
});
export default NumberSelected;