import React from 'react';
import { TouchableOpacity, TouchableNativeFeedback ,View, Text, StyleSheet, Platform}  from 'react-native';
import Colors from '../constants/colors';

export const CustomButton = (props) => {
    let ButtonComponent = TouchableOpacity;
    if(Platform.OS === 'android' && Platform.Version >= 21 ){
        ButtonComponent = TouchableNativeFeedback;
    }
    return (
        <ButtonComponent  activeOpacity={0.6} onPress={() => props.press()} style={{...styles.touchableContainer, ...props.buttonStyle}}>
            <View style={styles.buttonContainer}>
                <Text style={styles.buttonText}>{props.children}</Text>
            </View>
        </ButtonComponent>
    )
}
const styles = StyleSheet.create({
    touchableContainer:{
        borderRadius: 25,
        flexDirection: 'row', 
        justifyContent: "space-between",
    },
    buttonContainer: {
    },
    buttonText: {
        color: 'white',
        fontSize: 25
    },
});
export default CustomButton;