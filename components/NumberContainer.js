import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Colors from '../constants/colors';

export const NumberContainer = (props) => {
    const {guessNumber} = props;
    return (
        <View style={styles.container}>
            <Text style={styles.number}>{guessNumber}</Text>
        </View>
    )
}
const styles = StyleSheet.create({
    container:{
        borderWidth: 2,
        borderColor: Colors.warning,
        padding: 10,
        borderRadius: 10,
        marginVertical: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
    number: {
        color: Colors.success,
        fontSize: 22
    }
});
export default NumberContainer;