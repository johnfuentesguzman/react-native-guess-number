import React from 'react'
import {TextInput, StyleSheet, View,Text} from 'react-native'

export const Input = (props) => {
    const {styleSheet, val, length, numberInputHandler,keyboard, capitalize, correct} = props;
    return (
        <TextInput
          style={{...styles.input, ...styleSheet}}
          maxLength = {length}
          autoCorrect = {correct}
          placeholder="Type here your number!" 
          autoCapitalize={capitalize}
          keyboardType= {keyboard}
          blurOnSubmit
          value = {val}
          onChangeText={text => numberInputHandler(text)} 
        />
    )
}

const styles = StyleSheet.create({
    input: {
        borderBottomWidth: 1,
        borderBottomColor: 'black', 
        textAlign: 'center'
    }
});
export default Input;
