export const randomNumberBetween = (min,max,exclude) =>{
    min = Math.ceil(min); // return the smallest number
    max = Math.floor(max); // returns the greatest number
    const randomNumber = Math.floor (Math.random() * (max - min)) + min; // returns a random number between minimum and maximun parameters
    if(randomNumber === exclude){
        return randomNumberBetween(min,max,exclude) // if we have the same random as result than the number to exclude, then call the funtion again to get a new one
    } else {
        return randomNumber;
    }

};

export default randomNumberBetween;