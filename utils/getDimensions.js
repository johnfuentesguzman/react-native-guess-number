import { Dimensions } from 'react-native';

export const getDimensions = () => {
    const dim = Dimensions.get('screen')
    return  dim.height >= dim.width ? 'portrait' : 'landscape'

};

export default getDimensions;